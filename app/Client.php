<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Client extends Model
{

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'name', 'lastName', 'email', 'phone'
    ];


    /**
     * Get the loan requests record associated with the client.
    */
    public function client(){
        return $this->hasToMany('App\Client');
    }

}
