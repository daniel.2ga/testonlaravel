<?php

namespace App\Http\Controllers;

use App\Client;
use App\LoanRequest;
use App\User;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Validator;

class LoanRequestController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $loanRequests = LoanRequest::with(['client'])->get();

        $loanRequests->each(function ($loanRequest) {
            $loanRequest->percent();
        });

        return response()->json($loanRequests);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $data = $request->all();
        if (!empty($data)) {
            $validator = Validator::make($request->all(), [
                'name' => 'required',
                'email' => 'required',
                'phone' => 'required',
                'savings' => 'required|min:0',
                'price' => 'required|min:0',
            ]);

            if ($validator->fails()) {

                $data = array(
                    'status' => 'error',
                    'code' => 404,
                    'message' => 'Error al crear la solicitud',
                    'errors' => $validator->errors()
                );
            } else {

                $client = Client::updateOrCreate([
                    'name' => $data['name'],
                    'lastName' => $data['lastName'],
                    'email' => $data['email'],
                    'phone' => $data['phone']
                ]);

                $loanRequest = LoanRequest::create([
                    'client_id' => $client->id,
                    'savings' => $data['savings'],
                    'price' => $data['price'],
                    'assigned_user' => User::inRandomOrder()->first()->id
                ]);

                $loanRequest->percent();
                $loanRequest->client;

                $data = array(
                    'status' => 'success',
                    'code' => 200,
                    'message' => 'La solicitud se ha creado correctamente',
                    'loan_request' => $loanRequest
                );
            }
        } else {
            $data = array(
                'status' => 'error',
                'code' => 404,
                'message' => 'Los datos enviados no son correctos',
            );
        }
        return response()->json($data, $data['code']);
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\LoanRequest  $loanRequest
     * @return \Illuminate\Http\Response
     */
    public function show(LoanRequest $loanRequest)
    {
        $loanRequest->percent();
        $loanRequest->client;

        return response()->json($loanRequest);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\LoanRequest  $loanRequest
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, LoanRequest $loanRequest)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\LoanRequest  $loanRequest
     * @return \Illuminate\Http\Response
     */
    public function destroy(LoanRequest $loanRequest)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function getLoanRequestsByUserAndDates(Request $request)
    {
        $userId = (int) $request->userId;
        $from = \Date($request->from);
        $to = \Date($request->to);

        $loanRequests = LoanRequest::with(['client'])->where('assigned_user', $userId)->whereBetween('created_at', [$from, $to])->get();

        $loanRequests->each(function ($loanRequest) {
            $loanRequest->percent();
        });

        return response()->json($loanRequests);
    }
}
