<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class LoanRequest extends Model
{
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'client_id', 'savings', 'price', 'assigned_user'
    ];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        'client_id'
    ];

    /**
     * Get the client record associated with the loan request.
    */
    public function client(){
        return $this->belongsTo('App\Client');
    }

    public function percent(){
        return $this->percent =$this->savings / $this->price * 100;
    }
}
