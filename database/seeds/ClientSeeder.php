<?php

use Illuminate\Database\Seeder;
use Faker\Factory as Faker;


class ClientSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $faker = Faker::create('es_ES');
        for ($i = 0; $i < 5; $i++) {
            $gender = $faker->randomElement(['male', 'female']);
            \DB::table('clients')->insert(array(
                'name' => $faker->firstName($gender),
                'lastName' => $faker->lastName,
                'email' => $faker->unique()->safeEmail,
                'phone' => $faker->unique()->phoneNumber,
                'created_at' => date('Y-m-d H:m:s'),
                'updated_at' => date('Y-m-d H:m:s')
            ));
        }
    }
}
