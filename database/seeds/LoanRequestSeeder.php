<?php

use App\Client;
use App\User;
use Illuminate\Database\Seeder;
use Faker\Factory as Faker;

class LoanRequestSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $faker = Faker::create('es_ES');
        $clients = Client::all();
        $users = User::all();
        for ($i = 0; $i < 5; $i++) {
            $savings = $faker->randomFloat(2,0,300000.00);
            $date = $faker->dateTimeBetween($startDate = '-7 weeks', $endDate = 'now');
            \DB::table('loan_requests')->insert(array(
                'client_id' => $faker->randomElement( $clients )->id,
                'savings' => $savings,
                'price' => $savings + $faker->randomFloat(2,0,3000000.00),
                'assigned_user' => $faker->randomElement( $users )->id,
                'created_at' => $date,
                'updated_at' => $date
            ));
        }
    }
}
