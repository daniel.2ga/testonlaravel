<?php

namespace Tests\Feature;

use App\User;
use Illuminate\Foundation\Testing\RefreshDatabase;
use Illuminate\Foundation\Testing\WithFaker;
use Tests\TestCase;

class LoanRequestTest extends TestCase
{

    use RefreshDatabase;

    /**
     *
     * @return void
     */
    public function testCanCreateLoanRequest()
    {
        $user = factory(User::class)->create();

        $data = [
            'savings' => 259403.63,
            'price' => 2256154.82,
            'name' => 'Marcos',
            'lastName' => 'Zambrano',
            'email' => 'ismael.paredesaaaa@example.com',
            'phone' => '+34 929248046'
        ];

        $response = $this->post(route('loan_requests.store'), $data);

        $response->assertOk();
    }

    /**
     *
     * @return void
     */
    public function testGetLoanRequest()
    {
        $response = $this->get(route('loan_requests.index'));

        $response->assertOk();
    }

    /**
     *
     * @return void
     */
    public function testGetLoanRequestByFilters()
    {
        $response = $this->get('/api/loan_requests/user/2/from/2020-01-01/to/2020-01-31');

        $response->assertOk();
    }
}
